<?php

namespace ViniciusTest\Project\Tools\Task\Context;

use Exception;
use PDO;
use PDOException;
use PHPUnit\Framework\TestCase;
use Vinicius\Project\Tools\Core\Context\ContextInterface;
use Vinicius\Project\Tools\Kernel;
use Vinicius\Project\Tools\Task\Context\ListTaskContext;
use Vinicius\Project\Tools\Task\Repository\TaskRepository;

class Task
{
  public int $id;
  public string $name;
}

/**
 * @covers ListTaskContext
 * @package ViniciusTest\Project\Tools\Task\Context
 */
class ListTaskContextTest extends TestCase
{
  private Kernel $kernel;
  private TaskRepository $repo;
  private ListTaskContext $listTaskContext;

  protected function setUp(): void
  {
    $this->kernel = new Kernel();
    $this->repo = $this->kernel->container()->get(TaskRepository::class);
    $this->listTaskContext = $this->kernel->container()->get(ListTaskContext::class);

    $this->repo->createTable();
  }

  protected function tearDown(): void
  {
    $this->repo->dropTable();
  }

  public function test_context_execute()
  {
    $this->listTaskContext->execute();
    $result = $this->listTaskContext->result;
    $this->assertEquals([],$result);
    $this->assertInstanceOf(ContextInterface::class, $this->listTaskContext);
  }
}
