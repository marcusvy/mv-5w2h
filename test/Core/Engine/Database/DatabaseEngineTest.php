<?php

namespace ViniciusTest\Project\Tools\Core\Engine\Database;

use PDO;
use PHPUnit\Framework\TestCase;
use Vinicius\Project\Tools\Core\Engine\Database\DatabaseEngine;

/**
 * @covers Database
 * @package ViniciusTest\Project\Tools\Engine\Database
 */
class DatabaseEngineTest extends TestCase
{

  public function test_database_engine(): void
  {
    $dsn = 'sqlite:'.getcwd().'/data/database.db';
    $d = new DatabaseEngine();
    $d->setDsn($dsn);
    // $d->connect($dsn);

    $this->assertTrue($d->getConnection() instanceof PDO);
    $d->close();
  }
}
