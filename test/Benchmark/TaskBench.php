<?php
namespace ViniciusTest\Project\Tools\Benchmark;

use Exception;
use PDOException;
use Vinicius\Project\Tools\Kernel;
use Vinicius\Project\Tools\Task\Context\ListTaskContext;
use Vinicius\Project\Tools\Task\Repository\TaskRepository;

class TaskBench
{
  public function benchStart()
  {
    $kernel = new Kernel();

    /** @var ListTaskContext $context */
    $context = $kernel->container()->get(ListTaskContext::class);
    $conn = $kernel->getDatabase()->getConnection();

    /** @var TaskRepository $repo */
    $repo = $kernel->container()->get(TaskRepository::class);
    //faz a consulta
    try {
      $repo->dropTable();
      $repo->createTable();
      $repo->insert(['name' => "Registro Teste", 'is_done' => 1]);
      // $this->assertEquals($result->id, 1);
      // $this->assertEquals($result->name, 'Tal coisa');

      // Deleta Tabela
    } catch (PDOException $e) {
      throw new Exception($e->getMessage());
    }
  }
}
