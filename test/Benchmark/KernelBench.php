<?php
namespace ViniciusTest\Project\Tools\Benchmark;

use Vinicius\Project\Tools\Kernel;

class KernelBench
{
  public function benchCreateKernel()
  {
    $kernel = new Kernel();
  }

  public function benchConnectDatabaseWithKernel()
  {
    $kernel = new Kernel();
    $kernel->getDatabase()->getConnection();
  }
}
