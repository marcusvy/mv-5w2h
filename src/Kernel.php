<?php
namespace Vinicius\Project\Tools;

use DI\Container;
use DI\ContainerBuilder;
use Psr\Container\ContainerInterface;
use Vinicius\Project\Tools\Core\Engine\Database\DatabaseEngine;
use Vinicius\Project\Tools\Core\Engine\Database\DatabaseEngineInterface;
use Whoops\Handler\PlainTextHandler;
use Whoops\Run;

class Kernel
{

  private Container $container;

  private DatabaseEngineInterface $database;

  public function __construct()
  {
    $this->buildErrorHandler();
    $this->buildContainer();
    $this->buildDatabaseEngine();
  }

  private function buildErrorHandler()
  {
    $errorHandler = new Run();
    $errorHandler->appendHandler(new PlainTextHandler());
    $errorHandler->register();
  }

  private function buildContainer()
  {
    $builder = new ContainerBuilder();
    $this->container = $builder->build();
  }

  private function buildDatabaseEngine()
  {
    $dsn = 'sqlite:'.__DIR__.'/../data/database.db';
    $this->database = $this->container->get(DatabaseEngine::class);
    $this->database->setDsn($dsn);
    $this->container->set(DatabaseEngineInterface::class, $this->database);
  }

  /**
   * Get the value of container
   */
  public function container(): ContainerInterface
  {
    return $this->container;
  }

  public function setContainer(string $name, $value): void {
    $this->container->set($name, $value);
  }

  /**
   * Get the value of database
   */
  public function getDatabase()
  {
    return $this->database;
  }
}
