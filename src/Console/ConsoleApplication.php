<?php

namespace Vinicius\Project\Tools\Console;

use Symfony\Component\Console\Application;
use Vinicius\Project\Tools\Kernel;
use Vinicius\Project\Tools\Task\Console\Command\ListCommand;

class ConsoleApplication
{
  private Application $app;

  protected Kernel $kernel;

  /**
   * @var string[]
   */
  private array $commands = [
    ListCommand::class
  ];

  public function __construct()
  {
    $this->kernel = new Kernel();
    $this->app = new Application('Vinicius Toolset');
    $this->app->addCommands($this->getCommands());
  }

  public function run()
  {
    $this->app->run();
  }

  private function getCommands(): array
  {
    return array_map(fn ($c) => $this->kernel->container()->get($c), $this->commands);
  }
}
