<?php
namespace Vinicius\Project\Tools\Core\Context;

interface ContextInterface
{
  public function execute(): void;
}