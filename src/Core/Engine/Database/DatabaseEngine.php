<?php

namespace Vinicius\Project\Tools\Core\Engine\Database;

use Exception;
use PDO;

class DatabaseEngine implements DatabaseEngineInterface
{
  private string $dsn = '';
  protected $manager = null;
  protected ?PDO $conn = null;

  public function connect(string $dsn): void
  {
    if(empty($dsn)){
      throw new Exception("Config->Database->Dsn: O DSN não foi configurado");
    }
    // $dsn = 'sqlite:'.__DIR__.'/../data/database.db';
    $this->conn = new PDO($dsn);
  }

  public function close(): void
  {
    $this->conn = null;
  }

  /**
   * Get the value of conn
   */
  public function getConnection(): PDO
  {
    if(is_null($this->conn)){
      $this->connect($this->dsn);
    }
    return $this->conn;
  }

  /**
   * Set DSN
   * @param string $dsn
   * @return DatabaseEngineInterface
   */
  public function setDsn(string $dsn): DatabaseEngineInterface
  {
    $this->dsn = $dsn;
    return $this;
  }
}
