<?php
namespace Vinicius\Project\Tools\Core\Engine\Database;

use PDO;

interface DatabaseEngineInterface
{
  /**
   * Start a connection
   * @return void
   */
  public function connect(string $dsn): void;

  /**
   * Close a connection
   * @return void
   */
  public function close(): void;

  /**
   * Get the value of conn
   */
  public function getConnection(): PDO;

  /**
   * Config DSN
   * @param string $dsn
   * @return DatabaseEngineInterface
   */
  public function setDsn(string $dsn): DatabaseEngineInterface;
}