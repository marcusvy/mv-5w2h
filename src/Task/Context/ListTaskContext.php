<?php
namespace Vinicius\Project\Tools\Task\Context;

use Exception;
use Vinicius\Project\Tools\Core\Context\ContextInterface;
use Vinicius\Project\Tools\Task\Repository\TaskRepository;

class ListTaskContext implements ContextInterface
{
    private TaskRepository $repository;

    public mixed $result;
    public Exception $error;

    public function __construct(TaskRepository $repository)
    {
        $this->repository = $repository;
    }

    public function execute(): void {
        $result = $this->repository->fetchAll();
        $this->result = $result;
    }

}