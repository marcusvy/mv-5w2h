<?php

namespace Vinicius\Project\Tools\Task\Repository;

use PDO;
use PDOException;
use Vinicius\Project\Tools\Core\Engine\Database\DatabaseEngineInterface;

class TaskRepository
{
  private PDO $conn;

  public function __construct(DatabaseEngineInterface $database)
  {
    $this->conn = $database->getConnection();
  }

  public function createTable(): self
  {
    try {
      $sql = "create table task (
      id integer PRIMARY KEY AUTOINCREMENT,
      name varchar(200) NOT NULL,
      is_done integer(1) DEFAULT 1
    );";
      $this->conn->exec($sql);
    } catch (PDOException $e) {
    }
    return $this;
  }

  public function dropTable()
  {
    $table = 'task';
    try {
      $sql = "drop table $table";
      $this->conn->exec($sql);
    } catch (PDOException) {
    }
    return $this;
  }

  public function insert(array $values): bool
  {
    $table = 'task';
    $sqlColumns = implode(',', array_keys($values));
    $paramsColumns = array_map(fn ($p) => ":$p", array_keys($values));
    $sqlValues = implode(',', $paramsColumns);

    $sql = "insert into $table ($sqlColumns) values ($sqlValues)";
    $params = array_combine($paramsColumns, array_values($values));

    try {
      $stmt = $this->conn->prepare($sql);
      return $stmt->execute($params);
    } catch (PDOException $e) {
    }
    return false;
  }

  public function fetchAll()
  {
    $table = 'task';
    try {
      $sql = "select * from $table";
      $stmt = $this->conn->prepare($sql);
      $stmt->execute();
      $result = $stmt->fetchAll();
      return $result;
    } catch (PDOException $e) {
    }
  }
}
