<?php

namespace Vinicius\Project\Tools\Task\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Vinicius\Project\Tools\Core\Context\ContextInterface;
use Vinicius\Project\Tools\Task\Context\ListTaskContext;

class ListCommand extends Command
{
  protected static $defaultName = 'task:view';
  protected static $defaultDescription = 'List all tasks';
  private ?ContextInterface $context = null;

  public function __construct(ListTaskContext $context)
  {
    parent::__construct(self::$defaultName);
    $this->context = $context;
  }

  protected function configure()
  {
    $this->setHelp("List all tasks");
  }

  protected function execute(InputInterface $input, OutputInterface $output): int
  {
    $output->writeln([
      "Task :: List",
      str_repeat("=", 40)
    ]);

    $this->context->execute();

    return command::SUCCESS;
  }
}
